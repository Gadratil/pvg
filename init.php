<?php
function shutDownFunction() { 
    $error = error_get_last();
    // fatal error, E_ERROR === 1
    if ($error['type'] === E_ERROR) { 
        if (strpos($error['message'], 'escapeshellcmd()') !== false) {
          echo "Too long command, continuing...\n";
        } else {
          die($error['message']);
        }   
    } 
}
register_shutdown_function('shutDownFunction');