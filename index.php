<?php

define('VERY_BASE_DIR', __DIR__);

require 'init.php';

require 'vendor/autoload.php';

$runner = new PVG\Runner();

$runner->runPVG();