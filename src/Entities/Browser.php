<?php

namespace PVG\Entities;

class Browser {
    public $name = '';
    public $headers = [];
    public $viewportWidth = 0;
    public $viewportHeight = 0;
}