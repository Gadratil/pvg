<?php

namespace PVG\Entities;

class Proxy {
    public $ip = '';
    public $port = '';
    public $countryCode = '';
    public $countryName = '';
    public $type = '';
}