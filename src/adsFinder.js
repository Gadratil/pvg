var firstAd = jQuery('.adsbygoogle:visible').first();

var link = findFirstLink(firstAd);

link.trigger('click');

function findFirstLink(node) {
  var link = node.find('a');
  console.log(node);
  if ( !link.length ) {
    link = findFirstLink(node.find('iframe').first().contents());
  }

  return link;
}