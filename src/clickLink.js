// find link
var pattern = '||link-to-find||';
var links = document.getElementsByTagName('a');

var linkToClick = null;
for ( var i = 0; i < links.length; i++ ) {
  var currentLink = links[i];
  if ( currentLink.href == pattern ) {
    linkToClick = currentLink;
    break;
  } 
}

if ( linkToClick !== null ) {
  linkToClick.click();
} else {
  window.location = pattern;
}