<?php

namespace PVG\Proxy;

use PVG\Proxy\Adapter\OnlineListAdapter;
use PVG\Proxy\Adapter\GetProxyListAdapter;
use PVG\Proxy\Adapter\LocalListAdapter;
use PVG\Config;

class Loader {
  private static $_adapter = null;

  private static $_local_list_adapter = null;

  public static function loadRandomProxy() {
    if ( self::$_adapter === null ) {
      self::$_adapter = self::_setAdapter();
    }

    try {
      $proxy = self::$_adapter->load();
      if ( $proxy === null ) {
        echo "Cannot load proxy from adapter, falling back to list...\n";
        return self::_loadFromLocalList();
      }

      return $proxy;
    } catch (ProxyException $e) {
      echo "Cannot load proxy from adapter, falling back to list...\n";
      return self::_loadFromLocalList();
    }
  }

  private static function _setAdapter() {
    $adapter = Config::getConfig('proxyAdapter');
    return new $adapter();
  }

  /**
   * Gets called as backup function if the adapter cannot load a proxy
   *
   * @return PVG\Entities\Proxy;
   */
  private static function _loadFromLocalList() {
    if ( self::$_local_list_adapter === null ) {
      self::$_local_list_adapter = new LocalListAdapter();
    }

    return self::$_local_list_adapter->load();
  }
}