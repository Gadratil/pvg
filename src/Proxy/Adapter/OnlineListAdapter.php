<?php

namespace PVG\Proxy\Adapter;

use PVG\Entities\Proxy;
use PVG\Proxy\ProxyException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class OnlineListAdapter implements AdapterInterface {
  private static $_list = [];
  private static $_wasLoaded = false;
  
  public function load() {
    if ( !self::$_wasLoaded ) {
      $this->loadData();
    }

    if ( !empty(self::$_list) ) {
      $index = array_rand(self::$_list);

      $proxy = self::$_list[$index];

      array_splice(self::$_list, $index, 1);

      return $proxy;
    }

    return null;
  }

  private function loadData() {
    $client = new Client(['verify' => false]);
    echo "Loading online proxy list...\n";
    try{
      $res = $client->get('https://raw.githubusercontent.com/clarketm/proxy-list/master/proxy-list.txt');
      if ($res->getStatusCode() == 200) {
        $proxies = explode("\n",$res->getBody());

        if ( !empty($proxies) ) {
          for ( $i = 9; $i < count($proxies) - 2; $i++ ) {
            $line = $proxies[$i];
            if ( strpos($line, '-N') === false ) {
              // If anonymity exists
              $parts = explode(' ', $line);
              $address = explode(':', $parts[0]);
              $details = explode('-', $parts[1]);
              $proxy = new Proxy();
              $proxy->ip = $address[0];
              $proxy->port = $address[1];
              $proxy->countryCode = $details[0];
              $proxy->countryName = $details[0];
              $proxy->type = $details[1];

              self::$_list[] = $proxy;
            }
          }
          
          self::$_wasLoaded = true;
          echo "Loaded online proxy list...\n";
        }
      }
    } catch(RequestException $e) {
      // TODO: add logging of actual error
      throw new ProxyException('Proxy list could not be loaded');
    }
  }
}