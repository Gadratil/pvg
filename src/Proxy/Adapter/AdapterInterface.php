<?php

namespace PVG\Proxy\Adapter;

interface AdapterInterface {
  /**
   * Loads a proxy
   *
   * @return PVG\Entities\Proxy
   */
  public function load();
}