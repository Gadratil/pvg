<?php

namespace PVG\Proxy\Adapter;

use PVG\Entities\Proxy;
use PVG\Proxy\ProxyException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class GetProxyListAdapter implements AdapterInterface
{
  public function load()
  {
    $client = new Client(['verify' => false]);
    try{
      $res = $client->get('http://api.getproxylist.com/proxy');
      if ($res->getStatusCode() == 200) {
        $decoded = \json_decode($res->getBody());
        $proxy = new Proxy();
        $proxy->ip = $decoded->ip;
        $proxy->port = $decoded->port;
        $proxy->countryCode = $decoded->country;
        $proxy->countryName = $decoded->country;
        $proxy->type = $decoded->anonymity;

        return $proxy;
      } else {
        return null;
      }
    } catch(RequestException $e) {
      // TODO: add logging of actual error
      throw new ProxyException('Proxy could not be loaded');
    }
  }
}