<?php

namespace PVG\Proxy\Adapter;

use PVG\Config;

class LocalListAdapter implements AdapterInterface {
  public function load() {
    $list = Config::getConfig('proxy_list');
    $index = array_rand($list);
    $used_proxies = Config::getConfig('used_proxy_list');
    $used_proxies[] = $list[$index];

    $proxy = $list[$index];

    array_splice($list, $index, 1);

    Config::setConfig('proxy_list', $list);
    Config::setConfig('used_proxy_list', $used_proxies);

    return $proxy;
  }
}