<?php

namespace PVG;

use PVG\Entities\Proxy;
use PVG\Entities\Browser;
use HeadlessChromium\BrowserFactory;
use HeadlessChromium\Page;
use Exception;
use PVG\Config;

class Client {
    private $_client;
    private $_page;

    public function __construct(Proxy $proxy, Browser $browser){
        $browserFactory = new BrowserFactory(Config::getConfig('executable_name'));
        $this->_client = $browserFactory->createBrowser([
            'windowSize' => [$browser->viewportWidth, $browser->viewportHeight],
            'enableImages' => true,
            'ignoreCertificateErrors' => true,
            'connectionDelay' => 2.8,
            'sendSyncDefaultTimeout' => 7000,
            'userAgent' => $browser->headers['User-Agent'],
            'customFlags' => [
                '--proxy-server="' . $proxy->ip . ':' . $proxy->port . '"',
                '--disable-web-security'
           ]
        ]);
        
        $this->_page = $this->_client->createPage();
    }

    public function goToWebsite($url){
        try{
            $this->_page->navigate($url)->waitForNavigation(Page::NETWORK_IDLE, 60000);
        } catch(Exception $e) {
            $this->_page->close();
            throw $e;
        }
    
        file_put_contents('output.html', $this->_page->evaluate('document.documentElement.innerHTML')->getReturnValue());
    }

    public function clickLink($link) {
        $clickScript = \file_get_contents(VERY_BASE_DIR.'/src/clickLink.js');
        $clickScript = \str_replace('||link-to-find||', $link, $clickScript);
        try {
            $this->_page->evaluate($clickScript)->waitForPageReload(Page::LOAD, 60000);
        } catch(Exception $e) {
            $this->_page->close();
            throw $e;
        }
        file_put_contents('output.html', $this->_page->evaluate('document.documentElement.innerHTML')->getReturnValue());
    }

    public function clickAd() {
        // Find the first occurence of an ad and click it
        $this->_page->evaluate(file_get_contents(VERY_BASE_DIR.'/src/adsFinder.js'));
    }

    public function __destruct()
    {
        $this->_page->close();
    }
}