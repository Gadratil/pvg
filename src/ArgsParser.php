<?php

namespace PVG;

class ArgsParser {
  public static function parseArgs() {
    global $argc, $argv;
    $args = [];
    for( $i = 1; $i < sizeof($argv); $i++ ) {
      $parts = explode('=', $argv[$i]);
      $args[$parts[0]] = isset($parts[1]) ? $parts[1] : '';
    }

    return $args;
  }
}