<?php

namespace PVG;

use PVG\Entities\Proxy;
use PVG\Entities\Browser;
use PVG\ArgsParser;

/**
 * Config class for loading and managing configurable variables troughout the application
 */
class Config {
  private static $_proxy_list_file = 'proxy_list.csv';
  private static $_browser_list_file = 'src/Variables/browsers.json';
  private static $_paths_file = 'src/Variables/paths.json';

  private static $proxy_list = [];
  private static $used_proxy_list = [];
  private static $browsers = [];
  private static $paths = [];

  private static $views_to_generate = 200;
  private static $wait_times = [5, 6, 7, 8, 9];

  private static $args = [];

  private static $proxyAdapter = 'PVG\Proxy\Adapter\OnlineListAdapter';

  private static $executable_name = 'chrome';

  public static function loadConfig() {
    self::$args = ArgsParser::parseArgs();

    // Check what args we have and set them
    foreach( self::$args as $key => $value ) {
      if( !empty($value) ) {
        self::setConfig($key, $value);
      }
    }

    echo "Loading proxy list...\n";
    self::_loadProxyList();
    echo "Proxy list loaded...\n";

    echo "Loading browsers...\n";
    self::_loadBrowsers();
    echo "Loaded browsers\n";

    echo "Loading paths...\n";
    self::_loadPaths();
    echo "Loaded paths\n";
  }

  public static function getConfig($var = '') {
   if ( isset(self::$$var) ) {
     return self::$$var;
   }
  }

  public static function setConfig($var = '', $value) {
    if ( isset(self::$$var) ) {
      self::$$var = $value;
    }
  }

  private static function _loadProxyList()
  {
    $proxyString = file_get_contents(self::$_proxy_list_file);
    $lines = explode("\n", $proxyString);
    $list = [];
    foreach($lines as $line) {
      $data = explode(';', $line);
      $proxy = new Proxy();
      $proxy->ip = $data[0];
      $proxy->port = $data[1];
      $proxy->countryCode = isset($data[2])? $data[2] : '';
      $proxy->countryName = isset($data[3])? $data[3] : '';
      $proxy->type = isset($data[4])? $data[4] : '';
      $list[] = $proxy;
    }

    self::$proxy_list = $list;
  }

  private static function _loadBrowsers() {
    $json = file_get_contents(self::$_browser_list_file);
    $list = \json_decode($json);
    foreach ( $list as $browserRaw ) {
      $browser = new Browser();
      $browser->name = $browserRaw->name;
      $browser->headers = (array)$browserRaw->headers;
      $browser->viewportWidth = $browserRaw->viewportWidth;
      $browser->viewportHeight = $browserRaw->viewportHeight;

      self::$browsers[] = $browser;
    }
  }

  private static function _loadPaths() {
    $json = file_get_contents(self::$_paths_file);
    self::$paths = json_decode($json);
  }
}