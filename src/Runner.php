<?php

namespace PVG;

use Exception;
use PVG\Proxy\Loader as ProxyLoader;

class Runner {
    public function runPVG() {
        // Load up config
        Config::loadConfig();

        echo "Starting to go trough sites...\n";
        foreach ( Config::getConfig('paths') as $site ) {
            echo "Working on site: ".$site->site."\n";
            $runs = Config::getConfig('views_to_generate');
            echo "Iterating " . $runs . " times...\n";
            while ( !empty(Config::getConfig('proxy_list')) || $runs > 0 ) {
                try {
                    $proxy = ProxyLoader::loadRandomProxy();
                    echo "Using proxy: " .$proxy->ip. ":" .$proxy->port. " from " .$proxy->countryCode. " ...\n";
                    $browser = $this->_getRandomBrowser();
                
                    $client = new Client($proxy, $browser);
                    echo "Opening website ...\n";
                    $client->goToWebsite($site->site);
                    $this->_waitSomeTime();

                    $this->_traversePathsRecursive($client, $site->paths);
                    $runs--;
                } catch (Exception $e) {
                    echo "Error: " . $e->getMessage() . "\n";
                    echo "Connection error, skipping ...\n";
                }

                echo "Finished website...\n";
            }
        }
    }

    private function _getRandomBrowser() {
        $browsers = Config::getConfig('browsers');
        return $browsers[array_rand($browsers)];
    }

    private function _waitSomeTime() {
        $wait_times = Config::getConfig('wait_times');
        $seconds = $wait_times[array_rand($wait_times)];
        echo "Waiting " . $seconds ." seconds... \n";
        sleep($seconds);
    }

    private function _traversePathsRecursive(Client $client, Array $paths) {
        if ( !empty($paths) ) {
            $random_path = $paths[array_rand($paths)];
            if ( isset($random_path->link) ) {
                echo "Clicking link: " . $random_path->link . "\n";
                $client->clickLink($random_path->link);
                $this->_waitSomeTime();

                if ( isset($random_path->paths) ) {
                    $this->_traversePathsRecursive($client, $random_path->paths);
                }
            }

            if ( isset($random_path->clickAd) ) {
                echo "Clicking AD \n";
                $client->clickAd();
            }
        }
    }
}