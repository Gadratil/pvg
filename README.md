# Gadratil page view generator

This PHP script is for generating page views using proxy servers. It uses the headless Chrome browser to navigate the website. 
The code uses as much randomness as possible so the views are as random as possible.

# Usage
## Dependencies
You need PHP with minimal version of 5.6; You need chrome installed on the system and runnable with the command
```
chrome
```  
Also you need composer to install the php dependencies.
## Start
When you unzipped the archive, enter the folder from terminal or command line and install the dependencies:
```
composer install
```
This will install all the necessary dependencies.

To run the script, simply type:
```
php index.php
```

## Simple set-up
There are predefined files you can edit to customize how Page View Generator generates the views. The main file you need to edit is ```src/Variables/paths.json```
```
[
  {
    "site": "http://www.gadratilprogramming.net/",
      "paths": [
      {
        "link": "http://www.gadratilprogramming.net/projects/",
	"paths": [
	  {
	    "link": "http://www.gadratilprogramming.net/projects/web-applications",
            "paths": [
	      {
                "link": "http://www.gadratilprogramming.net/projects/web-applications/carwash/",
              },
              {
                "link": "http://www.gadratilprogramming.net/projects/web-applications/servicebox/",
              }
            ]
          },
          {
            "link": "http://www.gadratilprogramming.net/projects/microcontroller-programming",
            "paths": [
              {
                 "link": "http://www.gadratilprogramming.net/projects/microcontroller-programming/ad-system-based-on-raspberry-pi/",
              }
            ]
          }
        ]
      }
      ]
  }
]
```
The main object in the main array is a site. You can add multiple sites to the array.
Inside the array are the paths. The algorithm traverses the paths recursively, following the rules:
 - If there are multiple paths, it randomly chooses one, otherwise goes down on the one
 - If the chosen path has following paths, it goes deeper, until it reaches a point where there are no more paths
 - Every view goes down on a random path, like a user casually clicking on inner page links

By default, the code uses an open-source list of proxys, that is updated daily.
https://raw.githubusercontent.com/clarketm/proxy-list/master/proxy-list.txt
For every chrome window, a random browser header is chosen and a random proxy is used and on every page the code waits a random amount if seconds.
The headers that you can use can be found in ```src/Variables/browsers.json```

## Arguments

You can pass these arguments in the command line to change settings and what-not. The format is 
```
php index.php abc=value
```

|Argument|Default value|Description|
|----------------|-------------------------------|-----------------------------|
|_browser_list_file|src/Variables/browsers.json|You can specify a different json file with browser headers|
|_paths_file|src/Variables/paths.json|You can change the default paths file|
|views_to_generate|200|The number of views to generate in one run|
|proxyAdapter|PVG\Proxy\Adapter\OnlineListAdapter|The default adapter used to geth the proxy list from online. The other is PVG\Proxy\Adapter\LocalListAdapter that uses a local static file with proxys|
|_proxy_list_file|proxy_list.csv|This list is used with the local list adapter and you can use your own version based on the existing list|